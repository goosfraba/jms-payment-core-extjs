Ext.define('WebitPaymentJmsCore.model.FinancialTransactionType',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'label',
		type: 'string'
	}]
});
