Ext.define('WebitPaymentJmsCore.model.PaymentState',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'label',
		type: 'string'
	}]
});
