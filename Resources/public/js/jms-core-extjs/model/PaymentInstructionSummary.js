Ext.define('WebitPaymentJmsCore.model.PaymentInstructionSummary',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'payment_system_name',
		type: 'string'
	},{
		name: 'amount',
		type: 'float',
		useNull: true
	},{
		name: 'deposited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'currency',
		type: 'string'
	},{
		name: 'extended_data',
		defaultValue: {}
	},{
		name: 'state',
		type: 'int',
		useNull: true
	},{
		name: 'created_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	},{
		name: 'transaction_symbol',
		type: 'string'
	},{
		name: 'transaction_date',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	}]
});
