Ext.define('WebitPaymentJmsCore.model.FinancialTransaction',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'credit_id',
		type: 'int',
		useNull: true
	},{
		name: 'payment_id',
		type: 'int',
		useNull: true
	},{
		name: 'extended_data',
		defaultValue: {}
	},{
		name: 'processed_amount',
		type: 'float',
		useNull: true
	},{
		name: 'reason_code',
		type: 'string'
	},{
		name: 'reference_number',
		type: 'string'
	},{
		name: 'requested_amount',
		type: 'float',
		useNull: true
	},{
		name: 'response_code',
		type: 'string'
	},{
		name: 'state',
		type: 'int',
		useNull: true
	},{
		name: 'created_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	},{
		name: 'updated_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	},{
		name: 'tracking_id',
		type: 'string'
	},{
		name: 'transaction_type',
		type: 'int',
		useNull: true
	}],
	proxy: {
		type: 'webitrest',
		appendId: false,
		urlSelector: Webit.data.proxy.StoreUrlSelector('webit_payment_jms.financial_transaction_store'),
		reader: {
			type: 'json',
			root: 'data'
		},
		writer: {
			type: 'json',
			writeAllFields: true,
			allowSingle: false
		}
	}
	
});
