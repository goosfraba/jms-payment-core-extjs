Ext.define('WebitPaymentJmsCore.model.Payment',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'payment_instruction_id',
		type: 'int',
		useNull: true
	},{
		name: 'approved_amount',
		type: 'float',
		useNull: true
	},{
		name: 'approving_amount',
		type: 'float',
		useNull: true
	},{
		name: 'credited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'crediting_amount',
		type: 'float',
		useNull: true
	},{
		name: 'deposited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'depositing_amount',
		type: 'float',
		useNull: true
	},{
		name: 'expiration_date',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	},{
		name: 'reversing_approved_amount',
		type: 'float',
		useNull: true
	},{
		name: 'reversing_credited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'reversing_deposited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'state',
		type: 'int',
		useNull: true
	},{
		name: 'target_amount',
		type: 'float',
		useNull: true
	},{
		name: 'attention_required',
		type: 'bool',
		defaultValue: false
	},{
		name: 'expired',
		type: 'bool',
		defaultValue: false
	},{
		name: 'created_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	},{
		name: 'updated_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	}],
	hasMany: [{
		name: 'WebitPaymentJmsCore.model.FinancialTransaction'
	}],
	proxy: {
		type: 'webitrest',
		appendId: false,
		urlSelector: Webit.data.proxy.StoreUrlSelector('webit_payment_jms.payment_store'),
		reader: {
			type: 'json',
			root: 'data'
		},
		writer: {
			type: 'json',
			writeAllFields: true,
			allowSingle: false
		}
	}
});
