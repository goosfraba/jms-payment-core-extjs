Ext.define('WebitPaymentJmsCore.model.PaymentInstruction',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'amount',
		type: 'float',
		useNull: true
	},{
		name: 'approved_amount',
		type: 'float',
		useNull: true
	},{
		name: 'approving_amount',
		type: 'float',
		useNull: true
	},{
		name: 'created_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s',
		persist: false
	},{
		name: 'credited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'crediting_amount',
		type: 'float',
		useNull: true
	},{
		name: 'currency',
		type: 'string'
	},{
		name: 'deposited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'depositing_amount',
		type: 'float',
		useNull: true
	},{
		name: 'extended_data',
		defaultValue: {}
	},{
		name: 'payment_system_name',
		type: 'string'
	},{
		name: 'reversing_approved_amount',
		type: 'float',
		useNull: true
	},{
		name: 'reversing_credited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'reversing_deposited_amount',
		type: 'float',
		useNull: true
	},{
		name: 'state',
		type: 'int',
		useNull: true
	},{
		name: 'updated_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s',
		persist: false
	}],
	hasMany: [{
		name: 'WebitPaymentJmsCore.model.Payment'
	}],
	proxy: {
		type: 'webitrest',
		appendId: false,
		urlSelector: Webit.data.proxy.StoreUrlSelector('webit_payment_jms.payment_instruction_store'),
		reader: {
			type: 'json',
			root: 'data'
		},
		writer: {
			type: 'json',
			writeAllFields: true,
			allowSingle: false
		}
	}
});
