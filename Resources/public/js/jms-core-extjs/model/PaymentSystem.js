Ext.define('WebitPaymentJmsCore.model.PaymentSystem',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	},{
		name: 'name',
		type: 'string'
	},{
		name: 'label',
		type: 'string'
	},{
		name: 'enabled',
		type: 'boolean',
		defaultValue: false
	}],
	proxy: {
		type: 'webitrest',
		appendId: false,
		urlSelector: Webit.data.proxy.StoreUrlSelector('webit_payment_jms.payment_system_store'),
		reader: {
			type: 'json',
			root: 'data'
		},
		writer: {
			type: 'json',
			writeAllFields: true,
			allowSingle: false
		}
	}
});
