Ext.define('WebitPaymentJmsCore.util.Format',{
	statics: {
		paymentInstructionStateRenderer: function() {
			return function(v, meta, r) {
				switch(v) {
					case 1:
						return 'Zamknięta';
						break;
					case 2:
						return 'Niepoprawna';
					break;
					case 3:
						return 'Nowa';
					break;
					case 4:
						return 'Poprawna';
					break;
				}
				
				return v;
			}
		},
		paymentStateRenderer: function() {
			return function(v, meta, r) {
				switch(v) {
					case 1:
						return 'Zaakceptowana';
						break;
					case 2:
						return 'Akceptowanie';
					break;
					case 3:
						return 'Anulowana';
					break;
					case 4:
						return 'Wygasła';
					break;
					case 5:
						return 'Bład';
					break;
					case 6:
						return 'Nowa';
					break;
					case 7:
						return 'Wpłacanie';
					break;
					case 8:
						return 'Wpłacona';
					break;
				}
				
				return v;
			}
		},
		financialTransactionStateRenderer: function() {
			return function(v, meta, r) {
				switch(v) {
					case 1:
						return 'Anulowana';
						break;
					case 2:
						return 'Błąd';
					break;
					case 3:
						return 'Nowa';
					break;
					case 4:
						return 'Oczekująca';
					break;
					case 5:
						return 'Sukces';
					break;
				}
				
				return v;
			}
		},
		financialTransactionTypeRenderer: function() {
			return function(v, meta, r) {
				switch(v) {
					case 1:
						return 'Akceptacja';
						break;
					case 2:
						return 'Akceptacja i wpłata';
					break;
					case 3:
						return 'Kredyt';
					break;
					case 4:
						return 'Wpłata';
					break;
					case 5:
						return 'Akceptacja zwrotu';
					break;
					case 6:
						return 'Zwrot kredytu';
					break;
					case 7:
						return 'Zwrot wpłaty';
					break;
				}
				
				return v;
			}
		}
	}
});