Ext.define('WebitPaymentJmsCore.controller.Front',{
	extend: 'Ext.app.Controller',
	requires: [
		'WebitPaymentJmsCore.util.Format'
	],
	models: [
		'WebitPaymentJmsCore.model.FinancialTransaction',
		'WebitPaymentJmsCore.model.FinancialTransactionState',
		'WebitPaymentJmsCore.model.FinancialTransactionType',
		'WebitPaymentJmsCore.model.Payment',
		'WebitPaymentJmsCore.model.PaymentInstruction',
		'WebitPaymentJmsCore.model.PaymentInstructionState',
		'WebitPaymentJmsCore.model.PaymentState',
		'WebitPaymentJmsCore.model.PaymentSystem'
	],
//	stores: [
//		'WebitPaymentJmsCore.store.FinancialTransactionStateStore',
//		'WebitPaymentJmsCore.store.FinancialTransactionTypeStore',
//		'WebitPaymentJmsCore.store.PaymentInstructionStateStore',
//		'WebitPaymentJmsCore.store.PaymentStateStore'
//	],
	views: [
		'WebitPaymentJmsCore.view.PaymentInstructionPanel',
		'WebitPaymentJmsCore.view.PaymentInstructionGrid',
		'WebitPaymentJmsCore.view.PaymentInstructionForm',
		'WebitPaymentJmsCore.view.PaymentGrid',
		'WebitPaymentJmsCore.view.FinancialTransactionGrid',
		'WebitPaymentJmsCore.view.deposit.DepositForm',
		'WebitPaymentJmsCore.view.deposit.SimpleFormType',
		'WebitPaymentJmsCore.view.deposit.CashbillDirectFormType'
	],
	controllers: [
		'WebitPaymentJmsCore.controller.PaymentInstructionPanel',
		'WebitPaymentJmsCore.controller.PaymentInstructionForm'
	]
});