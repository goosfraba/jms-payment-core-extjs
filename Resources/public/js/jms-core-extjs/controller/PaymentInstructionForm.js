Ext.define('WebitPaymentJmsCore.controller.PaymentInstructionForm',{
	extend: 'Ext.app.Controller',
	init: function() {
		this.control({
			'webit_payment_jms_core_payment_instruction_form': {
				afterrender: this.onPaymentInstructionFormAfterRender
			},
			'webit_payment_jms_core_deposit_form': {
				afterrender: this.onDepositFormAfterRender
			}
		});
	},
	onPaymentInstructionFormAfterRender: function(formpanel) {
		formpanel.down('button[itemId="save"]').setHandler(this.paymentInstructionSaveHandler);
	},
	paymentInstructionSaveHandler: function(btn) {
		var formPanel = btn.up('form');
		if(formPanel.getForm().isValid() == false) {
			return false;
		}
		var r = formPanel.getForm().getRecord();
		formPanel.getForm().updateRecord(r);
		formPanel.getEl().mask('Zapisywanie...');
		r.save({
			params: formPanel.getSaveParams(),
			success: function(record, operation) {
				formPanel.getEl().unmask();
				formPanel.fireEvent('save', formPanel, record, operation);
			},
			failure: function(record, operation) {
				formPanel.getEl().unmask();
				Ext.Msg.alert('Nowa instrkucja płatności','Wystąpił błąd podczas dodawania instrukcji płatności.');
				formPanel.fireEvent('savefailure', formPanel, record, operation);
			}
		});
	},
	onDepositFormAfterRender: function(formpanel) {
		formpanel.down('button[itemId="deposit"]').setHandler(this.depositFormDepositHandler);
	},
	depositFormDepositHandler: function(btn) {
		var formPanel = btn.up('form');
		if(formPanel.getForm().isValid() == false) {
			return false;
		}
		
		var pi = formPanel.getPaymentInstruction();
		
		var ed = pi.get('extended_data') && Ext.isArray(pi.get('extended_data').data) == false ? pi.get('extended_data').data || {} : {};
		
		var values = formPanel.getValues();
		Ext.apply(ed,values);
		
		pi.set('extended_data',{data: ed});
		formPanel.getEl().mask('Rozliczanie...');
		pi.save({
			success: function(record, operation) {
				console.info(record);
				formPanel.getEl().unmask();
				formPanel.fireEvent('save', formPanel, record, operation);
			},
			failure: function(record, operation) {
				formPanel.getEl().unmask();
				Ext.Msg.alert('Rozliczanie operacji','Wystąpił błąd podczas rozliczania płatności.');
				formPanel.fireEvent('savefailure', formPanel, record, operation);
			}
		});
		
	}
});
