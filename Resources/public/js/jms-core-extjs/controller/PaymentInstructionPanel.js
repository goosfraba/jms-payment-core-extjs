Ext.define('WebitPaymentJmsCore.controller.PaymentInstructionPanel',{
	extend: 'Ext.app.Controller',
	init: function() {
		this.control({
			'webit_payment_jms_core_payment_instruction_grid': {
				selectionchange: this.onPaymentInstructionGridSelectionChange,
				afterrender: this.onPaymentInstructionGridAfterRender
			},
			'webit_payment_jms_core_payment_grid': {
				selectionchange: this.onPaymentGridSelectionChange,
				afterrender: this.onPaymentGridAfterRender
			}
		});
	},
	onPaymentInstructionGridAfterRender: function(grid) {
		grid.getStore().load({
			callback: function(records, operation, success) {
				if(success && records.length > 0) {
					grid.getSelectionModel().select(0);	
				}
			}
		});
		
		this.registerPaymentInstructionGridButtonHandlers(grid);
	},
	registerPaymentInstructionGridButtonHandlers: function(grid) {
		Ext.each(['add','delete','deposit'],function(itemId) {
			var btn = grid.down(Ext.String.format('button[itemId="{0}"]',itemId));
			
			if(btn) {
				var functionName = Ext.String.format('paymentInstruction{0}Handler',Ext.String.capitalize(itemId));
				btn.setHandler(this[functionName]);
			}
		},this);
	},
	onPaymentInstructionGridSelectionChange: function(sm, selection) {
		var grid = sm.view.ownerCt;
		var paymentGrid = grid.up('webit_payment_jms_core_payment_instruction_panel').down('webit_payment_jms_core_payment_grid');
		
		paymentGrid.getStore().clearFilter(true);
		if(selection.length == 1) {
			paymentGrid.getStore().filter({property: 'payment_instruction',value: selection[0].get('id'), type: 'numeric'});
			paymentGrid.enable();
		} else {
			paymentGrid.getStore().removeAll();
			paymentGrid.disable();
		}
		
		this.togglePaymentInstructionGridButtons(grid);
	},
	togglePaymentInstructionGridButtons: function(grid) {
		var sel = grid.getSelectionModel().getSelection();
		var btn = grid.down('button[itemId="delete"]');
		if(btn) {
			btn.setDisabled(sel.length != 1);
		}
		
		var btn = grid.down('button[itemId="deposit"]');
		var disable = sel.length != 1 || (sel[0].get('amount') - sel[0].get('deposited_amount')) == 0; 
		btn.setDisabled(disable);
	},
	onPaymentGridAfterRender: function(grid) {
		grid.getStore().on('load',function(store, records, success, eOpt) {
			if(records.length > 0) {
				grid.getSelectionModel().select(0);
			}
		});
	},
	onPaymentGridSelectionChange: function(sm, selection) {
		var grid = sm.view.ownerCt;
		var financialTransactionGrid = grid.up('webit_payment_jms_core_payment_instruction_panel').down('webit_payment_jms_core_financial_transaction_grid');
		financialTransactionGrid.getStore().clearFilter(true);
		if(selection.length == 1) {
			financialTransactionGrid.getStore().filter({property: 'payment',value: selection[0].get('id'), type: 'numeric'});
			financialTransactionGrid.enable();
		} else {
			financialTransactionGrid.getStore().removeAll();
			financialTransactionGrid.disable();
		}
	},
	paymentInstructionAddHandler: function(btn) {
		var piPanel = btn.up('webit_payment_jms_core_payment_instruction_panel');
		
		var defaults = btn.up('webit_payment_jms_core_payment_instruction_panel').getPaymentInstructionDefaults();
		var win = Ext.create('widget.window',{
			title: 'Dodaj instrukcję płatności',
			iconCls: 'fam-silk-add',
			renderTo: piPanel.getEl(),
			width: 400,
			layout: 'fit',
			items: [{
				xtype: 'webit_payment_jms_core_payment_instruction_form',
				saveParams: defaults['params'] || {}
			}]
		});
		
		win.down('webit_payment_jms_core_payment_instruction_form').on('save',function(fp, record, operation) {
			btn.up('grid').getStore().reload();
			win.close();
		});
		
		win.down('button[itemId="cancel"]').setHandler(function() {win.close()});
		
		var s = btn.up('grid').getStore();
		var className = Ext.ClassManager.getName(s.model);
		var pi = Ext.create(className,defaults['values']);
		win.down('webit_payment_jms_core_payment_instruction_form').getForm().loadRecord(pi);
		
		win.show();
	},
	paymentInstructionDeleteHandler: function(btn) {
		var piPanel = btn.up('webit_payment_jms_core_payment_instruction_panel');
		var grid = btn.up('grid');
		var sel = grid.getSelectionModel().getSelection();
		var r = sel.length == 1 ? sel[0] : null;
		
		if(r == null) {
			return false;
		}
		
		Ext.Msg.confirm('Usuwanie instrukcji płatności','Czy na pewno chcesz usunąć wybraną instrukcję płatności?',function(btnId) {
			if(btnId == 'yes') {
//				grid.getEl().mask('Usuwanie...');
				r.destroy({
					success: function(rec, operation) {
//						grid.getEl().unmask();
					},
					failure: function(rec, operation) {
						grid.getEl().unmask();
						Ext.Msg.alert('Usuwanie instrukcji płatności','Wystąpił błąd podczas usuwanie instrukcji płatności.');
					}
				})
			}
		});
	},
	paymentInstructionDepositHandler: function(btn) {
		var piPanel = btn.up('webit_payment_jms_core_payment_instruction_panel');
		var grid = btn.up('grid');
		var sel = grid.getSelectionModel().getSelection();
		var r = sel.length == 1 ? sel[0] : null;
		
		if(r == null) {
			return false;
		}
		
		var depositFormType = Ext.String.format('webit_payment_jms_core_deposit_{0}_form',r.get('payment_system_name'));
		var win = Ext.create('widget.window',{
			title: 'Rozliczanie płatności',
			iconCls: 'fam-silk-coins',
			width: 270,
			layout: 'fit',
			items: [{
				xtype: 'webit_payment_jms_core_deposit_form',
				paymentSystemDepositFormType: depositFormType,
				paymentInstruction: r
			}]
		});
		win.down('webit_payment_jms_core_deposit_form').on('save',function(fp, record, operation) {
			grid.getSelectionModel().deselectAll();
			grid.getSelectionModel().select(record);
			win.close();
		});
		
		win.down('button[itemId="cancel"]').setHandler(function(btn) {win.close()});
		win.show();
	}
});