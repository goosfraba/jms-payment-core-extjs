Ext.define('WebitPaymentJmsCore.view.FinancialTransactionGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.webit_payment_jms_core_financial_transaction_grid',
	initComponent: function() {
		Ext.apply(this,{
			columns: [{
				dataIndex: 'id',
				header: 'ID',
				hidden: true
			},{
				header: 'Kwota',
				columns: [{
					xtype: 'numbercolumn',
					format: '0,00/i',
					dataIndex: 'requested_amount',
					header: 'Żądana',
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					dataIndex: 'processed_amount',
					header: 'Przetworzona',
					width: 150
				}]
			},{
				dataIndex: 'reference_number',
				header: 'Symbol',
				width: 200
			},{
				header: 'Odpowiedź',
				hidden: true,
				columns: [{
					dataIndex: 'reason_code',
					header: 'Przyczyna',
					hidden: true,
					width: 150
				},{
					dataIndex: 'response_code',
					header: 'Kod',
					hidden: true,
					width: 150
				}]
			},{
				dataIndex: 'state',
				header: 'Status',
				width: 150,
				renderer: WebitPaymentJmsCore.util.Format.financialTransactionStateRenderer()
			},{
				dataIndex: 'tracking_id',
				header: 'Numer śledzenia',
				width: 150,
				hidden: true
			},{
				dataIndex: 'transaction_type',
				header: 'Typ',
				hidden: true,
				width: 150,
				renderer: WebitPaymentJmsCore.util.Format.financialTransactionTypeRenderer()
			},{
				dataIndex: 'created_at',
				header: 'Utworzono',
				width: 120,
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			},{
				dataIndex: 'updated_at',
				header: 'Zmodyfikowano',
				minWidth: 140,
				flex: 1,
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			}],
			store: {
				model: 'WebitPaymentJmsCore.model.FinancialTransaction',
				remoteFilter: true,
				remoteSort: false,
				remoteGroup: true,
				sorters: [{
					property: 'updated_at',
					direction: 'DESC'
				},{
					property: 'id',
					direction: 'DESC'
				}]
			}
		});
		this.callParent();
	}
});
