Ext.define('WebitPaymentJmsCore.view.PaymentInstructionGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.webit_payment_jms_core_payment_instruction_grid',
	initComponent: function() {
		Ext.apply(this,{
			columns: [{
				header: 'ID',
				dataIndex: 'id',
				hidden: true
			},{
				header: 'System płatności',
				dataIndex: 'payment_system_name',
				width: 200
			},{
				header: 'Kwota',
				columns: [{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Całkowita',
					dataIndex: 'amount',
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Zaakceptowana',
					dataIndex: 'approved_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Wpłacona',
					dataIndex: 'deposited_amount',
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Skredytowana',
					dataIndex: 'credited_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Akceptowana',
					dataIndex: 'approving_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Wpłacana',
					dataIndex: 'depositing_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Kredytowana',
					dataIndex: 'crediting_amount',
					hidden: true,
					width: 150
				}]
			},{
				header: 'Kwota zwrotu',
				hidden: true,
				columns: [{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Zaakceptowana',
					dataIndex: 'reversing_approved_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Wpłacona',
					dataIndex: 'reversing_credited_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Kredytowana',
					dataIndex: 'reversing_deposited_amount',
					hidden: true,
					width: 150
				}]
			},{
				header: 'Waluta',
				dataIndex: 'currency'
			},{
				header: 'Status',
				dataIndex: 'state',
				width: 150,
				renderer: WebitPaymentJmsCore.util.Format.paymentInstructionStateRenderer()
			},{
				header: 'Utworzono',
				dataIndex: 'created_at',
				width: 120,
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			},{
				header: 'Zmodyfikowano',
				dataIndex: 'updated_at',
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'),
				minWidth: 120,
				flex: 1
			}],
			dockedItems: [{
				dock: 'right',
				xtype: 'toolbar',
				items: [{
					iconCls: 'fam-silk-add',
					itemId: 'add',
					tooltip: 'Nowa instrukcja'
				},{
					iconCls: 'fam-silk-delete',
					itemId: 'delete',
					tooltip: 'Usuń',
					disabled: true
				},{
					iconCls: 'fam-silk-coins',
					itemId: 'deposit',
					tooltip: 'Rozlicz',
					disabled: true
				}]
			}]
		});
		
		if(this.store == null) {
			Ext.apply(this,{
				store: {
					model: 'WebitPaymentJmsCore.model.PaymentInstruction',
					remoteFilter: true,
					remoteSort: false,
					remoteGroup: false,
					sorters: [{
						property: 'updated_at',
						direction: 'DESC'
					},{
						property: 'id',
						direction: 'DESC'
					}]
				}
			});
		}
		this.callParent();
	}
});
