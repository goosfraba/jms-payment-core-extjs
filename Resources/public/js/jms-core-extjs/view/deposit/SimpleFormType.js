Ext.define('WebitPaymentJmsCore.view.deposit.SimpleFormType',{
	extend: 'Ext.panel.Panel',
	alias: [
		'widget.webit_payment_jms_core_deposit_simple_cash_form',
		'widget.webit_payment_jms_core_deposit_simple_bank_transfer_form',
		'widget.webit_payment_jms_core_deposit_simple_cod_form'
	],
	border: false,
	frame: true,
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'textfield',
				name: 'deposit_symbol',
				fieldLabel: 'Symbol',
				labelWidth: 60,
				allowBlank: false,
				width: 250
			},{
				xtype: 'numberfield',
				name: 'amount',
				fieldLabel: 'Kwota',
				decimalSeparator: ',',
				labelWidth: 60,
				width: 150,
				anchor: null,
				hideTrigger: true,
				allowBlank: false
			},{
				xtype: 'datefield',
				name: 'deposit_date',
				fieldLabel: 'Data',
				anchor: null,
				format: 'Y-m-d H:i:s',
				labelWidth: 60,
				width: 220
			},{
				xtype: 'hidden',
				name: 'status',
				value: 'ok'
			}]
		});
		
		this.callParent();
	},
	getDefaultValues: function(paymentInstruction, amount) {
		return {
			amount: amount
		}
	},
	getValues: function() {
		var values = this.up('form').getForm().getFieldValues();
		if(values['deposit_date']) {
			values['deposit_date'] = Ext.util.Format.date(values['deposit_date'],'Y-m-d H:i:s');	
		} else {
			values['deposit_date'] = null
		}
		
		return values;
	}
})