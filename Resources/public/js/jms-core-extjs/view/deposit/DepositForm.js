Ext.define('WebitPaymentJmsCore.view.deposit.DepositForm',{
	extend: 'Ext.form.Panel',
	alias: 'widget.webit_payment_jms_core_deposit_form',
	config: {
		paymentSystemDepositFormType: null,
		paymentInstruction: null
	},
	layout: 'fit',
	initComponent: function() {
		this.addEvents('save','savefailure');
		
		Ext.apply(this,{
			border: false,
			items: [{
				xtype: this.paymentSystemDepositFormType
			}],
			buttons: [{
				text: 'Rozlicz',
				iconCls: 'fam-silk-coins',
				itemId: 'deposit'
			},{
				text: 'Anuluj',
				iconCls: 'fam-silk-cross',
				itemId: 'cancel'
			}]
		});
		
		this.callParent();
		
		var values = this.getDefaultValues(this.getPaymentInstruction());
		this.getForm().setValues(values);
	},
	getDefaultValues: function(paymentInstruction) {
		var amount = paymentInstruction.get('amount') - paymentInstruction.get('deposited_amount');
		return this.items.get(0).getDefaultValues(paymentInstruction, amount);
	},
	getValues: function() {
		return this.items.get(0).getValues();
	}
});