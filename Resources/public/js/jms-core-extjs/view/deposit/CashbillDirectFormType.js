Ext.define('WebitPaymentJmsCore.view.deposit.CashbillDirectFormType',{
	extend: 'Ext.panel.Panel',
	alias: [
		'widget.webit_payment_jms_core_deposit_cashbill_direct_form'
	],
	border: false,
	frame: true,
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'textfield',
				name: 'orderid',
				fieldLabel: 'Symbol',
				labelWidth: 60,
				width: 250
			},{
				xtype: 'numberfield',
				name: 'amount',
				fieldLabel: 'Kwota',
				decimalSeparator: ',',
				labelWidth: 60,
				hideTrigger: true,
				width: 150,
				anchor: null
			},{
				xtype: 'hidden',
				name: 'status',
				value: 'ok'
			}]
		});
		
		this.callParent();
	},
	getDefaultValues: function(paymentInstruction, amount) {
		return {
			amount: amount
		}
	},
	getValues: function() {
		var values = this.up('form').getForm().getFieldValues();
		
		return values;
	}
})