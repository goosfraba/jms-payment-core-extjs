Ext.define('WebitPaymentJmsCore.view.PaymentGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.webit_payment_jms_core_payment_grid',
	initComponent: function() {
		Ext.apply(this,{
			columns: [{
				header: 'ID',
				dataIndex: 'id',
				hidden: true
			},{
				header: 'Kwota',
				columns: [{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Docelowa',
					dataIndex: 'target_amount',
					width: 150
				},{
					header: 'Zaakceptowana',
					xtype: 'numbercolumn',
					format: '0,00/i',
					dataIndex: 'approved_amount',
					hidden: true,
					width: 150
				},{
					header: 'Wpłacona',
					xtype: 'numbercolumn',
					format: '0,00/i',
					dataIndex: 'deposited_amount',
					width: 150
				},{
					header: 'Skredytowana',
					xtype: 'numbercolumn',
					format: '0,00/i',
					dataIndex: 'credited_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Akceptowana',
					dataIndex: 'approving_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Wpłacana',
					dataIndex: 'depositing_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Kredytowana',
					dataIndex: 'crediting_amount',
					hidden: true,
					width: 150
				}]
			},{
				header: 'Kwota zwrotu',
				hidden: true,
				columns: [{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Zaakceptowana',
					dataIndex: 'reversing_approved_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Wpłacona',
					dataIndex: 'reversing_credited_amount',
					hidden: true,
					width: 150
				},{
					xtype: 'numbercolumn',
					format: '0,00/i',
					header: 'Kredytowana',
					dataIndex: 'reversing_deposited_amount',
					hidden: true,
					width: 150
				}]
			},{
				header: 'Status',
				dataIndex: 'state',
				width: 150,
				renderer: WebitPaymentJmsCore.util.Format.paymentStateRenderer()
			},{
				header: 'Wygasła',
				dataIndex: 'expired',
				width: 100,
				renderer: function(v) {
					return v ? 'Tak' : 'Nie';
				}
			},{
				header: 'Wygasa dnia',
				dataIndex: 'expiration_date',
				width: 140,
				hidden: true,
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			},{
				header: 'Utworzono',
				dataIndex: 'created_at',
				width: 120,
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			},{
				header: 'Zmodyfikowano',
				dataIndex: 'updated_at',
				minWidth: 140,
				flex: 1,
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			}],
			store: {
				model: 'WebitPaymentJmsCore.model.Payment',
				remoteFilter: true,
				remoteSort: true,
				remoteGroup: true,
				sorters: [{
					property: 'updated_at',
					direction: 'DESC'
				},{
					property: 'id',
					direction: 'DESC'
				}]
			}
		});
		
		this.callParent();
	}
});
