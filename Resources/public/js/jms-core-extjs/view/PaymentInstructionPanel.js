Ext.define('WebitPaymentJmsCore.view.PaymentInstructionPanel',{
	extend: 'Ext.panel.Panel',
	alias: 'widget.webit_payment_jms_core_payment_instruction_panel',
	border: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	config: {
		paymentInstructionStore: null,
		paymentInstructionDefaults: {
			values: {},
			params: {}
		}
	},
	defaults: {
		flex: 1
	},
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'webit_payment_jms_core_payment_instruction_grid',
				title: 'Instrukcje',
				store: this.paymentInstructionStore
			},{
				xtype: 'webit_payment_jms_core_payment_grid',
				title: 'Płatności',
				disabled: true
			},{
				xtype: 'webit_payment_jms_core_financial_transaction_grid',
				title: 'Transakcje',
				disabled: true
			}]
		});
		
		this.callParent();
	}
});