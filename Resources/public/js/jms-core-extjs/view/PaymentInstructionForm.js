Ext.define('WebitPaymentJmsCore.view.PaymentInstructionForm',{
	extend: 'Ext.form.Panel',
	alias: 'widget.webit_payment_jms_core_payment_instruction_form',
	border: false,
	frame: true,
	config: {
		saveParams: {}
	},
	initComponent: function() {
		this.addEvents('save','savefailure');
		Ext.apply(this,{
			items: [{
				xtype: 'combo',
				fieldLabel: 'System płatności',
				name: 'payment_system_name',
				anchor: '100%',
				store: Ext.create('Ext.data.Store',{
					data: Webit.data.StaticData.getData('payment_system'),
					model: 'WebitPaymentJmsCore.model.PaymentSystem'
				}),
				editable: false,
				forceSelection: true,
				allowBlank: false,
				filters: [{
					property: 'enabled',
					value: true
				}],
				remoteFilter: false,
				remoteSort: false,
				queryMode: 'local',
				valueField: 'name',
				displayField: 'label'
			},{
				xtype: 'numberfield',
				fieldLabel: 'Kwota',
				minValue: 0.01,
				name: 'amount',
				decimalSeparator: ',',
				width: 200
			},{
				xtype: 'combo',
				fieldLabel: 'Waluta',
				name: 'currency',
				width: 170,
				store: Ext.create('Ext.data.Store',{
					fields: [{name: 'id'}],
					data: [{id: 'PLN'}]
				}),
				valueField: 'id',
				displayField: 'id',
				forceSelection: true,
				editable: false,
				allowBlank: false
			}],
			buttonAling: 'right',
			buttons: [{
				text: 'Zapisz',
				iconCls: 'fam-silk-tick',
				itemId: 'save'
			},{
				text: 'Anuluj',
				iconCls: 'fam-silk-cross',
				itemId: 'cancel'
			}]
		});
		
		this.callParent();
	}
});
