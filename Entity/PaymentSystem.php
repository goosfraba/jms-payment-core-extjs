<?php

namespace Webit\Accounting\PaymentJmsCoreExtJsBundle\Entity;

class PaymentSystem {
	
	/**
	 *
	 * @var int
	 */
	protected $id;
	
	/**
	 *
	 * @var string
	 */
	protected $name;
	
	/**
	 *
	 * @var string
	 */
	protected $label;
	
	/**
	 *
	 * @var bool
	 */
	protected $enabled = false;
	
	/**
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 *
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}
	
	/**
	 *
	 * @param string $label
	 */
	public function setLabel($label) {
		$this->label = $label;
	}
	
	/**
	 *
	 * @return bool
	 */
	public function getEnabled() {
		return $this->enabled;
	}
	
	/**
	 *
	 * @param bool $enabled
	 */
	public function setEnabled($enabled) {
		$this->enabled = $enabled;
	}
}