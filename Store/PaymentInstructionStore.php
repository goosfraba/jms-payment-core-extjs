<?php
namespace Webit\Accounting\PaymentJmsCoreExtJsBundle\Store;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreInterface;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterParamsInterface;
use JMS\Payment\CoreBundle\PluginController\PluginControllerInterface;
use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;
use JMS\Payment\CoreBundle\PluginController\Result;

class PaymentInstructionStore implements ExtJsStoreInterface
{
	/**
	 * 
	 * @var PluginControllerInterface
	 */
	protected $pc;
	
    /**
     * @var EntityManager
     */
    protected $em;
    
    public function __construct(PluginControllerInterface $pc, EntityManager $em)
    {
    	$this->pc = $pc;
        $this->em = $em;
    }

    public function getOption($option)
    {
        return null;
    }

    public function getModelList($queryParams,
            FilterCollectionInterface $filters,
            SorterCollectionInterface $sorters, $page = null,
            $limit = null, $offset = null) {

        $qb = $this->em->getRepository($this->getDataClass())->createQueryBuilder('pi');                
		$total = $qb->select('COUNT(DISTINCT pi.id) as num')->getQuery()->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
		
		$qb->select('pi');
		$qbDec->applySorters($sorters);
		$qbDec->applyLimit($limit);
		$qbDec->applyOffset($offset);

        $arPi = $qb->getQuery()->execute();

        $json = new ExtJsJson();
            $json->setData($arPi);
            $json->setTotal($total);
            $json->setSerializerGroups(array('Default'));

        return $json;
    }

    public function loadModel($id, $queryParams)
    {
        $order = $this->em->getRepository($this->getDataClass())->find($id);
		$json = new ExtJsJson();
			$json->setData($order);
			$json->setSerializerGroups(array('Default'));
		
		return $json;
    }

    public function createModels(\Traversable $modelListData)
    {
        // TODO: Auto-generated method stub

    }

    public function createModel($model)
    {
        // TODO: Auto-generated method stub

    }

    public function updateModels(\Traversable $modelListData)
    {
    	$arPi = array();
		foreach($modelListData as $pi) {
			$ePi = $this->em->getRepository($this->getDataClass())->find($pi->getId());
			if($ePi == null) {
				
			}
			
			$transaction = $ePi->getPendingTransaction();
			if($transaction) {
				$transaction->setState(FinancialTransactionInterface::STATE_CANCELED);
				$transaction->getPayment()->setState(PaymentInterface::STATE_CANCELED);
				$this->em->flush(array($transaction, $transaction->getPayment()));
			}
			
			$arEd = $pi->getExtendedData()->all();
			$amount = !isset($arEd['amount']) ? ($ePi->getAmount() - $ePi->getDepositedAmount()) : $arEd['amount'];
			
			foreach($pi->getExtendedData()->all() as $key=>$value) {
				if($ePi->getExtendedData()->has($key) == false) {
					$ePi->getExtendedData()->set($key, $value);
				}
			}
			
			$payment = $this->pc->createPayment($ePi->getId(), $amount);
			$result = $this->pc->approveAndDeposit($payment->getId(), $payment->getTargetAmount());
			
			if(Result::STATUS_PENDING === $result->getStatus()) {
				$result = $this->pc->approveAndDeposit($payment->getId(), $payment->getTargetAmount());
			}
			
			if(Result::STATUS_SUCCESS === $result->getStatus() && $ePi->getAmount() == $ePi->getDepositedAmount()) {
				$this->pc->closePaymentInstruction($ePi);
			}
			
			$arPi[] = $ePi;
		}
		
        $json = new ExtJsJson();
        $json->setData($ePi);
        $json->setSerializerGroups(array('Default'));
        
        return $json;
    }

    public function updateModel($model)
    {
        // TODO: Auto-generated method stub

    }

    public function deleteModels(\Traversable $modelListData)
    {
        foreach($modelListData as $id) {
        	die(var_dump($id));
        }

    }

    public function deleteModel($id)
    {
    	foreach($id as $pi) {
    		$ePi = $this->em->getRepository($this->getDataClass())->find($pi->getId());
        	$this->em->remove($ePi);
        }
        
        $this->em->flush();

        $json = new ExtJsJson();
        $json->setData(array());
        $json->setSerializerGroups(array('Default'));
        
        return $json;
    }

    public function getDataClass()
    {
        return 'JMS\Payment\CoreBundle\Entity\PaymentInstruction';

    }
}
