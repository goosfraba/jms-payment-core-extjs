<?php
namespace Webit\Accounting\PaymentJmsCoreExtJsBundle\Store;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreInterface;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterParamsInterface;

class PaymentSystemStore implements ExtJsStoreInterface
{
    /**
     * @var EntityManager
     */
    protected $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getOption($option)
    {
        return null;
    }

    public function getModelList($queryParams,
            FilterCollectionInterface $filters,
            SorterCollectionInterface $sorters, $page = null,
            $limit = null, $offset = null) {

        $qb = $this->em->getRepository($this->getDataClass())->createQueryBuilder('p');
        
        $qbDec = new QueryBuilderDecorator($qb);
        $qbDec->applyFilters($filters);
		$total = $qb->select('COUNT(DISTINCT p.id) as num')->getQuery()->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
		
		$qb->select('p');
		$qbDec->applySorters($sorters);
		$qbDec->applyLimit($limit);
		$qbDec->applyOffset($offset);

        $arPi = $qb->getQuery()->execute();

        $json = new ExtJsJson();
            $json->setData($arPi);
            $json->setTotal($total);
            $json->setSerializerGroups(array('Default'));

        return $json;
    }

    public function loadModel($id, $queryParams)
    {
        $order = $this->em->getRepository($this->getDataClass())->find($id);
		$json = new ExtJsJson();
			$json->setData($order);
			$json->setSerializerGroups(array('Default'));
		
		return $json;
    }

    public function createModels(\Traversable $modelListData)
    {
        // TODO: Auto-generated method stub

    }

    public function createModel($model)
    {
        // TODO: Auto-generated method stub

    }

    public function updateModels(\Traversable $modelListData)
    {
    	

        $json = new ExtJsJson();
        $json->setData(array());
        $json->setSerializerGroups(array('Default'));
        
        return $json;
    }

    public function updateModel($model)
    {
        // TODO: Auto-generated method stub

    }

    public function deleteModels(\Traversable $modelListData)
    {
        // TODO: Auto-generated method stub

    }

    public function deleteModel($id)
    {
        // TODO: Auto-generated method stub

    }

    public function getDataClass()
    {
        return 'JMS\Payment\CoreBundle\Entity\PaymentSystem';

    }
}
