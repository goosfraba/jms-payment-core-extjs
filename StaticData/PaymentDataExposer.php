<?php
namespace Webit\Accounting\PaymentJmsCoreExtJsBundle\StaticData;
use Webit\Bundle\ExtJsBundle\StaticData\StaticDataExposerInterface;

use Doctrine\ORM\EntityManager;

class PaymentDataExposer implements StaticDataExposerInterface {
	
	/**
	 * 
	 * @var EntityManager
	 */
	private $em;
	
	public function __construct(EntityManager $em) {
		$this->em = $em;
	}
	
	public function getExposedData() {
		$data = array(
			'payment_instruction_state'=>array(
				array('id'=>1, 'label'=>'Zamknięta'),
				array('id'=>2, 'label'=>'Niepoprawna'),
				array('id'=>3, 'label'=>'Nowa'),
				array('id'=>4, 'label'=>'Poprawna')
			),
			'payment_state'=>array(
				array('id'=>1, 'label'=>'Zaakceptowana'),
				array('id'=>2, 'label'=>'Akceptowanie'),
				array('id'=>3, 'label'=>'Anulowana'),
				array('id'=>4, 'label'=>'Wygasła'),
				array('id'=>5, 'label'=>'Błąd'),
				array('id'=>6, 'label'=>'Nowa'),
				array('id'=>7, 'label'=>'Wpłacania'),
				array('id'=>8, 'label'=>'Wpłacona')
			),
			'financial_transaction_state'=>array(
				array('id'=>1, 'label'=>'Anulowana'),
				array('id'=>2, 'label'=>'Błąd'),
				array('id'=>3, 'label'=>'Nowa'),
				array('id'=>4, 'label'=>'Oczekująca'),
				array('id'=>5, 'label'=>'Sukces')
			),
			'financial_transaction_type'=>array(
				array('id'=>1, 'label'=>'Akceptacja'),
				array('id'=>2, 'label'=>'Akceptacja i wpłata'),
				array('id'=>3, 'label'=>'Kredyt'),
				array('id'=>4, 'label'=>'Wpłata'),
				array('id'=>5, 'label'=>'Akceptacja zwrotu'),
				array('id'=>6, 'label'=>'Zwrot kredytu'),
				array('id'=>7, 'label'=>'Zwrot wpłaty')
			)
		);
		$data['payment_system'] = $this->em->getRepository('Webit\Accounting\PaymentJmsCoreExtJsBundle\Entity\PaymentSystem')->findAll();
		
		return $data;
	}
}
