<?php
namespace Webit\Accounting\PaymentJmsCoreExtJsBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializerInterface;
use Webit\Tools\Object\ObjectUpdater;
use Webit\Ecommerce\OrderBundle\Order\OrderManagerConfiguration;

class LoadPaymentSystemCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    
    /**
     * 
     * @var SerialierInterface
     */
    private $serializer;
    
    protected function configure()
    {
        parent::configure();
        $this->setName('webit:payment:load-payment-system')
            ->setDescription('Load payment systems');
    }

    /**
     * (non-PHPdoc)
     * @see Symfony\Component\Console\Command.Command::initialize()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->serializer = $this->getContainer()->get('serializer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$inputFile = __DIR__.'/../Resources/static/payment-system.json';
    	if(is_file($inputFile) == false) {
    		$output->writeln('<error>Input file not found.</error>');
    		return false;
    	}
    	
    	$repo = $this->em->getRepository('Webit\Accounting\PaymentJmsCoreExtJsBundle\Entity\PaymentSystem');
    	$coll = $this->serializer->deserialize(file_get_contents($inputFile),'ArrayCollection<Webit\Accounting\PaymentJmsCoreExtJsBundle\Entity\PaymentSystem>','json');
    	foreach($coll as $ps) {
    		$ePs = $repo->findOneBy(array('name'=>$ps->getName()));
    		if($ePs) {
    			$ePs->setLabel($ps->getLabel());
    			$ePs->setEnabled($ps->getEnabled());
    		} else {
    			$this->em->persist($ps);
    		}
    		
    	}
    	$this->em->flush();
    }

}
